package com.zuitt.example;
import java.util.Scanner;
public class Activity {
    public static void main(String[] args) {
        String firstName, lastName;
        Double firstSubject, secondSubject, thirdSubject;

        Scanner scan = new Scanner(System.in);

        System.out.print("First name: ");
        firstName = scan.nextLine();

        System.out.print("Last name: ");
        lastName = scan.nextLine();

        System.out.print("First subject grade: ");
        firstSubject = scan.nextDouble();

        System.out.print("Second subject grade: ");
        secondSubject = scan.nextDouble();

        System.out.print("Third subject grade: ");
        thirdSubject = scan.nextDouble();

        double average = (firstSubject + secondSubject + thirdSubject) / 3;

        System.out.println("Good day, " + firstName + " " + lastName);
        System.out.print("Your grade average is: " + average);
    }
}
