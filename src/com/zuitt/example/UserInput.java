package com.zuitt.example;

import java.util.Scanner;

public class UserInput {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.print("Enter Username: ");

        String userName = scan.next();
        System.out.println("Username is " + userName);
    }
}
