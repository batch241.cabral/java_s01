package com.zuitt.example;

import java.util.Scanner;

public class TypeConversion {
    public static void main(String[] args) {
        System.out.println("How old are you?");
        Scanner scan = new Scanner(System.in);

        double age = new Double(scan.next());

        System.out.println("The age is " + age);

    }
}
